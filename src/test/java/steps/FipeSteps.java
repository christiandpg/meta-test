package steps;

import io.cucumber.java.Before;
import io.cucumber.java.es.Dado;
import io.cucumber.java.pt.Então;
import io.cucumber.java.pt.Quando;


import utils.Utils;
import io.restassured.path.json.JsonPath;

import static io.restassured.RestAssured.given;
import static org.hamcrest.CoreMatchers.equalTo;


public class FipeSteps extends Utils{

    private String makeCode;
    private int modelCode;
    private String yearCode;
    private Utils util;
    JsonPath jsonPathValidator;

    public FipeSteps(){
        this.util = new Utils();
    }

    @Before
    public void initTest() {
        this.response = given().when().get(util.url);
    }

    @Dado("que eu sei marca do carro")
    public void iKnowCarMake() {
        jsonPathValidator = this.response.jsonPath();
        this.makeCode = jsonPathValidator.get("find { it.nome == '"+Utils.getPropertyValues("make")+"' }.codigo");
    }

    @Dado("o modelo")
    public void vehicleModel(){
        this.response = given().when().get(util.url +"/"+this.makeCode+"/modelos");
        jsonPathValidator = this.response.jsonPath();
        this.modelCode = jsonPathValidator.get("modelos.find { it.nome == '"+Utils.getPropertyValues("model")+"' }.codigo");
    }

    @Dado("o ano do veículo")
    public void vehicleYear(){
        this.response = given().when().get(util.url +"/"+this.makeCode+"/modelos/" +this.modelCode+"/anos");
        jsonPathValidator = this.response.jsonPath();
        this.yearCode = jsonPathValidator.get("find { it.nome == '"+Utils.getPropertyValues("year")+"' }.codigo");
    }

    @Quando("eu realizar uma consulta")
    public void performSearch(){
        this.response = given().when().get(util.url +"/"+this.makeCode+"/modelos/" + this.modelCode+"/anos/" + this.yearCode);
        jsonPathValidator = this.response.jsonPath();
    }

    @Então("é possível obter o seu valor atual de mercado")
    public void checkCurrentValue(){
        this.response.then().assertThat().body("Valor", equalTo("R$ 59.766,00"));
        this.response.then().assertThat().body("CodigoFipe", equalTo("004381-8"));
    }

}
