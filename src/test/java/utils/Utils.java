package utils;

import java.io.IOException;
import java.io.InputStream;
import java.util.Properties;
import io.restassured.response.Response;

public class Utils {

    public Response response;
    public String url = "https://parallelum.com.br/fipe/api/v1/carros/marcas";

    public static String getPropertyValues(String name) {
        Properties prop = new Properties();
        String value = null;
        InputStream input = null;
        try {

            input = Thread.currentThread().getContextClassLoader().getResourceAsStream("fipe.properties");
            prop.load(input);
            value = prop.getProperty(name);
        } catch (IOException ex) {
            ex.printStackTrace();
        }
        return value;
    }

}
