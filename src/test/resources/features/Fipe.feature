#language:pt

  Funcionalidade:
    Obter o código FIPE e o valor de mercado de um determinado veículo

  Cenário: Obter o valor da tabela Fipe
    Dado que eu sei marca do carro
    E o modelo
    E o ano do veículo
    Quando eu realizar uma consulta
    Então é possível obter o seu valor atual de mercado
