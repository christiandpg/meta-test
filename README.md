### Meta - Test Automation Challenge
This is an automated test project to automate REST API and get some car details from FIPE API.
The Framework itself is easy to maintain, create and adapt new flows.


## PREREQUISITES
* Java 1.8
* Gradle on your local machine **[Instructions to download and installation](https://gradle.org/install/)**
* Any IDE of your preference


## USAGE
You can choose run through any IDE (IDEA Intellij / Eclipse / ...).

To run through IDE, you can run use the file **Runner.java** inside utils package.


## WALKTHROUGH
The relevant information to run the framework is stored at **[fipe.properties](https://gitlab.com/christiandpg/meta-test/-/blob/master/src/test/resources/fipe.properties)** file.
This file contains the following configuration:

* **make** - make of a given vehicle
* **model** - model of a given vehicle
* **year** - year of a given vehicle

 
### THE FRAMEWORK
This is the project organization. The default organization of a new Gradle project was used.

```sh
├── build.gradle
└── src
    └── test
        │── java
        │   ├── steps
        │   └── utils
        │      
        └── resources
            └── features
```

* **test**
  * **java**
    * **steps** - all step definitions
    * **utils** - a bunch of classes with some utilities for the project
  * **resources** 
    * **features** - location of feature files
  

## BUILT WITH
* [Java](https://www.java.com/en/) - Programming language
* [JUnit](https://junit.org/junit5) - Unit testing framework for the Java programming language
* [Rest Assured](http://rest-assured.io) - Testing framework tool to validate REST services in Java 
* [Gradle](https://gradle.org/) - Build-automation system that builds upon the concepts of Apache Ant and Apache Maven

